% ------------------------------ PACKAGES ------------------------------

\documentclass{article}
\usepackage{fullpage}

\usepackage[french]{babel}

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{tikz}
\usepackage{graphics}

% ------------------------------ MACROS ------------------------------

\newcommand{\includetikzpicture}[1]{\includegraphics{../graphics/#1/#1.pdf}}

\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\norm}[1]{\abs{\abs{#1}}}
% ------------------------------ DOCUMENT ------------------------------

\begin{document}

% -------------------- Title --------------------

\title{An overview of GANs}
\author{Quentin Aristote \& Clément Ogier}
\date{}

\maketitle

% -------------------- Body --------------------

Generatory Adversarial Networks (GANs) are a family of unsupervised neural networks whose aim is to generate new objects alike to the ones they were given as input. For example, a GAN could be fed with a set of faces, and its goal would then be to generate completely new faces without a human being able to decide whether the face was part of the original set or whether it was generated by the GAN.

\section{Generatory Adversarial Networks}

\subsection{Architecture of a GAN}

Let $\mathcal{X}$ be the input space, and $p_{data}$ the distribution of training data among this space. Given a latent space $\mathcal{Z} = \mathbb{R}^n$ and a probability distribution $p_z$ on $\mathcal{Z}$, the goal is to find a function $G : \mathcal{Z} \rightarrow \mathcal{X}$ (the \emph{Generator}) such that $p_g(x) = p_z(G^{-1}(x))$ is as good an approximation of $p_{data}(x)$ as possible. To do so, the Generator is chosen to be a multilayer perceptron.

A second multilayer perceptron $D : \mathcal{X} \rightarrow \mathbb{R}$ (the \emph{Discriminator}) is introduced with the aim of being able to tell whether the data $x$ it was given as input is real data (\emph{i.e.} $p_{data}(x) > 0$) or whether it was generated by $G$ (\emph{i.e.} $x = G(z)$ for some $z \in \mathcal{Z}$). The Discriminator replaces an objective way of telling whether the generated data is good or not, as would be possible in a supervised learning context.

The hope is then that if the Generator and the Discriminator are trained alternatively, the Generator will get better at generating fake data by getting better at fooling the Discriminator, which will itself be getting better at telling real data from generated data.

In practice, GANs work well for vision problems when both the Generator and the Discriminator are made of convolutional layers.

% \begin{figure}[h]
%   \centering
%   \includetikzpicture{gan_architecture}
%   \caption{Architecture of a GAN}
%   \label{fig:gan_architecture}
% \end{figure}

\subsection{Training loss}

In the original paper, the Discriminator is assumed to output the probability that its input belonged to the set of real data. The Generator is then trained to minimize the following loss function :
\[ \max_D V(D,G) = \max_D \mathbb{E}_{x \sim p_{data}}[\log D(x)] + \mathbb{E}_{z \sim p_z}[\log (1 - D(G(z))] \]
which amounts to minimizing the Jensen-Shannon divergence between $p_{data}$ and $p_g$. In practice, the Discriminator is trained for a few iterations to approximate $\max_D V(D,G)$ for the current $G$, and the Generator is then trained to minimize $V(D,G)$ for the current $D$.

But when $D(G(z))$ is close to $0$ (for example at the beginning of the training process when the job is easy for the Discriminator), the gradients relative to $G$ are quite small  (they \emph{vanish}), slowing down the learning process. This problem can be solved in several ways, for example by having the Generator minimize
\[ \max_D \mathbb{E}_{x \sim p_{data}}[\log D(x)] - \mathbb{E}_{z \sim p_z}[\log D(G(z))] \]
which still amounts to minimizing the Jensen-Shannon divergence, or by having $D$ and $G$ respectively minimize (for a given constant $a$)
\begin{align*}
  &\mathbb{E}_{x \sim p_{data}}[(D(x) - a)^2] + \mathbb{E}_{z \sim p_z}[(D(G(z)) - a + 2)^2] \\
  &\mathbb{E}_{z \sim p_z}[(D(G(z)) - a + 1)^2]
\end{align*}
which amounts to minimizing another divergence, the Pearson divergence.

Although a divergence (whether it be Jensen-Shannon's or Pearson's) between $p_{data}$ and $p_g$ is minimal when $p_g = p_{data}$, this only guarantees that the solutions to the previous optimization problems are correct, but it does not ensure that a gradient descent will converge. Indeed, for such a result to be true, the loss function has to be continuous everywhere in the parameters of $G$, which does not necessarily hold true for a divergence.

A better choice is thus to try to minimize the Wasserstein distance, which is continuous in the parameters of $G$. It is defined by
\[ W(p_{data},p_g) = \inf_{\gamma \in \Pi(p_{data},p_g)} \mathbb{E}_{x,y \sim \gamma}[\norm{x - y}] \]
where the infimum is taken over all probability distributions over $\mathcal{X}^2$ with marginals $p_{data}$ and $p_g$. It measures how much $p_g$ has to be deformed to be transformed into $p_{data}$. This distance can also be rewritten as
\[ W(p_{data},p_g) = \sup_{\norm{f}_L \le 1} \mathbb{E}_{x \sim p_{data}}[f(x)] - \mathbb{E}_{z \sim p_z}[f(G(z))] \]
where the supremum is taken over all $1$-Lipschitz continuous function. The Discriminator can then be trained to be proportional to the optimal $f$ in the previous expression by ascending along the gradient of $\mathbb{E}_{x \sim p_{data}}[D(x)] - \mathbb{E}_{z \sim p_z}[D(G(z))]$ while keeping $D$ $K$-Lipschitz continuous for a given constant $K$, and the Generator can now be trained with $D$ as its loss function to minimize the Wasserstein distance.

\section{Improved training}

The simplest way to keep $D$ $K$-Lipschitz continuous, the simplest way is to clip every parameter of $D$ into $[-K,K]$. Although this leads to reasonably good results, it can hinder the learning process by causing the gradients to vanish or explode exponentially and by decreasing the ranks of the matrices between each consecutive layers. Fortunately, other methods to enforce Lipschitz continuity exist that do not suffer these drawbacks.

\subsection{Gradient Penalization}

The first way to do so relies on the fact that if a differentiable function $f$ maximizes
\[ \mathbb{E}_{x \sim p_{data}}[f(x)] - \mathbb{E}_{z \sim p_z}[f(G(z))] \]
among all $K$-Lipschitz continuous functions, than it has gradient norm $1$ almost everywhere under any convex combination of $p_{data}$ and $p_g$. This means that, for any given $\lambda$, training $D$ to minimize
\[ \mathbb{E}_{z \sim p_z}[D(x)] - \mathbb{E}_{x \sim p_{data}}[D(G(z))] + \lambda \mathbb{E}_{\substack{u \sim \mathcal{U}([0,1]) \\ x \sim up_{data} + (1 - u)p_g}}[(\norm{\nabla D(x)}_2 - 1)^2 \]
amounts to training $D$ to approximate the optimal $f$ in the expression of the Wasserstein distance, 
\subsection{Spectral Normalization}

While gradient penalization does not suffer from the problems mentioned above, it is unstable with high learning rates as the regularization term depends heavily on $p_g$, which changes throughout the training process. It also requires an additional round of forward and backward propagation to compute the norm of the gradient, making it computationally heavy. Spectral Normalization is another method solving those problems. It consists in dividing the matrices between consecutive layers by their spectral norms which also happen to be their Lipschitz constants. This is computationally lighter because the spectral norm can be approximated using the power method, and does not rely on $p_g$ at all.

% % -------------------- Bibliography --------------------
% \bibliographystyle{plain}
% \bibliography{review.bib}

\end{document}