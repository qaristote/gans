### Net Architecture

5 conv layers

SN: 5 power iterations

### Optimizer

Using Adam:
- lr = 0.0001
- b_1 = 0.5 (except = 0 for WGANs)
- b_2 = 0.999

### Training

train for 5 epochs over MNIST

WGAN: train discr more than gen (here x2)


