### GAN

    CPU times: user 4min 48s, sys: 1min 17s, total: 6min 6s
    Wall time: 6min 8s

### WGAN

    CPU times: user 3min 52s, sys: 57.9 s, total: 4min 50s
	Wall time: 4min 52s
	
### SN-GAN:

    CPU times: user 6min 11s, sys: 1min 2s, total: 7min 14s
    Wall time: 7min 15s

### SN-WGAN

    CPU times: user 9min 18s, sys: 3min 7s, total: 12min 25s
    Wall time: 12min 31s
	
### LSGAN

    CPU times: user 2min 47s, sys: 30.7 s, total: 3min 17s
    Wall time: 3min 19s

### WGAN-GP

    CPU times: user 3min 17s, sys: 45.1 s, total: 4min 3s
    Wall time: 4min 4s
